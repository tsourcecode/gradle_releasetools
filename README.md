#USAGE

Create file release.properties: 

```
#keystore filename (located in same dir as release.properties file)
STORE_FILE=your_app.keystore
STORE_PASSWORD=KEYSTORE_PASSWORD
KEY_ALIAS=KEYSTORE_ALIAS
KEY_PASSWORD=ALIAS_PASSWORD
#folder name where releases will be copied (located in root dir of project)
RELEASE_DIR=RELEASES_DIRECTORY
RELEASE_FLAVOR=GRADLE_RELEASE_FLAVOR
```

link to it inside your app's builg.gradle:

```
apply from:'../gradle_ReleaseTools/release-tools.gradle'

...

android {
	...

    signingConfigs {
        releaseConfig {
            storeFile = project.releaseToolsConfig.storeFile
            storePassword = project.releaseToolsConfig.storePassword
            keyAlias = project.releaseToolsConfig.keyAlias
            keyPassword = project.releaseToolsConfig.keyPassword
        }
    }

    buildTypes {
        release {
            signingConfig signingConfigs.releaseConfig
        }
    }

    ...
}

...
```

`gradle_releasetools/make_clean_release.sh` will assemble release apk and put it to releases folder with versioncode suffix