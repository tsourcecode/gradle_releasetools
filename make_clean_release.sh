set -e
#!/bin/sh
clear
echo "====> CLEANING:"
#./gradlew clean --quiet
echo "====> ASSEMBLING RELEASE:"
./gradlew assembleRelaseAPK # --quiet
#for debug
#./gradlew assembleRelease --info
echo "====> MOVING RELEASE APK:"
./gradlew renameReleaseAPK --quiet
echo "RELEASE ASSEMBLING DONE!"